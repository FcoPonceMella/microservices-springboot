package com.fp.webservices.restfullwebservices.exception;

import java.util.Date;

public class ExceptionDetails {
	//Tiempo en que ocurre la exception
	private Date timestamp;
	//Mensaje de la exception
	private String message;
	//Detalles de la exception
	private String details;
	
	//Constructor
	public ExceptionDetails(Date timestamp, String message, String details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getDetails() {
		return details;
	}
	
	
}
